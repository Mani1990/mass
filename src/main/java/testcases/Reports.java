package testcases;

import java.io.IOException;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class Reports {
	public static ExtentReports extent;
	public static ExtentTest test;
	public static String testCaseName, testCaseDescritption, author, category;
	


	@BeforeSuite //one time execution
	
	public void startResult() {
		
		        ExtentHtmlReporter html =new ExtentHtmlReporter("./reports/result.html");
				html.setAppendExisting(true);
                ExtentReports extent =new ExtentReports();
                extent.attachReporter(html);}
	
	@BeforeTest
	
	public void startTestCases() {

test = extent.createTest(testCaseName, testCaseDescription);
test.assignAuthor("author");
test.assignCategory("category");



public void reportStep(String desc, String status ) {
	if (status.equalsIgnoreCase("pass"));{
	test.pass(desc);
}if (status.equalsIgnoreCase("fail")) {
	
	test.fail(desc);
}
	@AfterSuite
	
	public void stopresult() {
	extent.flush();
	
}
}
