package wdMethods;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;



public class Projectmethods extends SeMethods {
	
	@BeforeMethod

	public void login() {
		
		startApp("chrome", "http://leaftaps.com/opentaps");
		
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, "DemoSalesManager");
		WebElement elePass = locateElement("id", "password");
		type(elePass, "crmsfa");
		WebElement login = locateElement("class", "decorativeSubmit");
		click(login);
		WebElement crm = locateElement("linktext", "CRM/SFA");
		click(crm);}
	
		
		@AfterMethod
		
		public void close()
		{
		
		
		closeAllBrowsers();
	}

}
